package com.app.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.model.OrderDetailModel;
import com.app.model.OrderModel;
import com.app.repository.OrderDetailRepository;
import com.app.repository.OrderRepository;

@RestController
@RequestMapping("api/listorder")
@CrossOrigin(origins = "*")
public class ListController {
	@Autowired
	private OrderRepository or;
	
	@Autowired
	private OrderDetailRepository odr;
	
	@GetMapping("listorder")
	public List<OrderModel> listorder(){
		return or.listorder();
	}
	@GetMapping("listorderdetail/{id}")
	public List<Map<String, Object>> listorderdetail(@PathVariable long id){
		return odr.listorderdetail(id);
	}

}
