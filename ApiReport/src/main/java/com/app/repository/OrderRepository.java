package com.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.model.OrderModel;

public interface OrderRepository extends JpaRepository<OrderModel, Long>{
	@Query(value ="select * from t_orders", nativeQuery = true)
	List<OrderModel>listorder();
}
