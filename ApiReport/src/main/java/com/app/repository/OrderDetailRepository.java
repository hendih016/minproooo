package com.app.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.model.OrderDetailModel;

public interface OrderDetailRepository extends JpaRepository<OrderDetailModel, Long> {
	@Query(value = "select o.id as idorder,o.amounts,o.note_number,o.order_date,o.customer_name,o.store_name, od.id as idorderdetatail,od.product_name,od.quantity,od.price,od.amount from t_orders o join t_order_details od on o.id=od.order_id where od.order_id= :id",nativeQuery = true)
	List<Map<String, Object>>listorderdetail(long id);	
}
