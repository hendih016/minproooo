package com.app.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class TabAlamatControl {

	@GetMapping("viewtabalamat")
	public String viewTabAlamat() {
		return "/profile/tabalamat";
	}
	
	@GetMapping("addalamat")
	public String addalamat() {
		return "/profile/addtabalamat";
	}
	
	@GetMapping("updatealamat")
	public String updatealamat() {
		return "/profile/updatetabalamat";
	}
	
	@GetMapping("deletealamat")
	public String deletealamat() {
		return "/profile/deletetabalamat";
	}
}
