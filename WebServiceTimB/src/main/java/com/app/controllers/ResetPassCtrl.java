package com.app.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
@Controller
public class ResetPassCtrl {

	@GetMapping("resetpassword")
	public String resetpassword() {
		return "resetpassword/reset";
	}
	
	@GetMapping("veritifikasipass")
	public String veritifikasipass() {
		return "resetpassword/veritifikasi_token";
	}
	
	@GetMapping("newpass")
	public String newpass() {
		return "resetpassword/NewPass";
	}
	
}
