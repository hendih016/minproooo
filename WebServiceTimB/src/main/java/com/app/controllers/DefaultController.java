package com.app.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class DefaultController {
	@GetMapping("/")
	public String dashboard() {
		return "dashboard";
	}
	
	@GetMapping("/dashboardlogin")
	public String dashlogin(HttpSession session, Model model) {
		//cek sesion variabel dengan nama userlogin
		Object userlogin = session.getAttribute("userlogin");
		
		if(userlogin==null)userlogin=new String();
		String id = userlogin.toString();
		
		if(userlogin.equals("")) {
			return "dashboard";
		}else {
			model.addAttribute("AEmail", userlogin);
			System.out.println("id"+userlogin);
			model.addAttribute("hId", id);
			return "dashboardlogin";
		}
	}
	
	

	@GetMapping("setsession/{id}")
	public String setsession(HttpServletRequest request,@PathVariable String id) {
			request.getSession().setAttribute("userlogin", id);
			return "redirect:/dashboardlogin";
	}
	
	@GetMapping("logout")
	public String logout(HttpServletRequest request) {
		request.getSession().invalidate();
		return "redirect:/";
	}
	
	  @GetMapping("signin/{id}") public void login(@PathVariable long id,
	  HttpServletRequest hs) { 
		  hs.getSession().setAttribute("userlogin", id);
	  
	  }
	 
	
	
	

}
