package com.app.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CariDoktorControl {
	@GetMapping("dummydokter")
	public String dummy() {
		return "caridokter/dummydokter.html";
	}

	@GetMapping("caridokter")
	public String cariDokter() {
		return "caridokter/caridokter.html";
	}
	
	@GetMapping("hasildokter")
	public String hasilCariDokter() {
		return "caridokter/hasildokter";
	}
	
	@GetMapping("detaildokter")
	public String detailDokter() {
		return "caridokter/detaildokter";
	}

}
