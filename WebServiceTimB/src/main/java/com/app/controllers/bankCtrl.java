package com.app.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class bankCtrl {

	@GetMapping("bank")
	public String bank() {
		
		//
		return "bank/bank";
	}
	
	@GetMapping("addbank")
	public String addbank() {
		return "bank/add";
	}
	
	@GetMapping("editbank")
	public String editbank() {
		return "bank/edit";
	}
	
	@GetMapping("deletebank")
	public String deletebank() {
		return "bank/delete";
	}
	
	
	
}
