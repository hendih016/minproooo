package com.app.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class profilecontrol {

	@GetMapping("profile")
	public String viewprofile() {
		return "/profile/profile";
	}
	@GetMapping("profile2")
	public String viewprofile2() {
		return "/profile/profile2";
	}
	@GetMapping("updatedatapribadi")
	public String viewdatapribadi() {
		return "/profile/updatedatapribadi";
	}
	@GetMapping("updateemail")
	public String viewupdateemail() {
		return "/profile/updateemail";
	}
	@GetMapping("otpemail")
	public String otpemail() {
		return "/profile/otp";
	}
	@GetMapping("currentpassword")
	public String viewupdatepassword() {
		return "/profile/currentpass";
	}
	@GetMapping("setpasswordnew")
	public String viewupdatepasswordnew() {
		return "/profile/updatepassword";
	}
}
