$(document).ready(function() {
	
	let dokterid=3
	sessionStorage.setItem("doctorid",dokterid);
	
	/*$.ajax({
		url: "http://localhost:81/api/currentdoctor/listbybiodata/"+biodataid,
		type: "GET",
		contentType: "application/json",
		success: function(hasil) {
			console.log(hasil.doctor_id)
			let dcId=hasil.doctor_id
			sessionStorage.setItem("doctorid",dcId)
		}
	})*/
		/*let dokterid=sessionStorage.getItem("doctorid");
		console.log(dokterid)*/
	$.ajax({
		url: "http://localhost:81/api/currentdoctor/list/" + dokterid,
		type: "GET",
		contentType: "application/json",
		success: function(hasil) {
			if (hasil.name == null) {
				
				$(".isispesialisasi").html("Anda Belum Menambahkan Spesialisasi")

				str = ""
				str += "<button type=\"button\" id=\"btnadd\" style = \"font-size: 20px; text-align: justify;\"class=\"add btn btn-primary btn \" > <i class=\"bi bi-plus-circle-fill\"></i></button > "
				$(".btnp").empty()
				$(".btnp").append(str)

				$(".add").click(function() {
					$.ajax({
						url: "tambahspesialisasi",
						type: "GET",
						dataType: "html",
						success: function(hasil) {
							$("#modalspesialisasi").modal('show')
							$(".isimodalspesialisasi").html(hasil)
						}
					})
					return false
				})

			} else {
				$(".isispesialisasi").html(hasil.name)

				str = ""
				str += "<button type=\"button\" id=\"btnedit\" style = \"font-size: 20px; text-align: justify;\"class=\"edit btn btn-primary btn \" ><i class=\"bi bi-pencil-fill\"></i> </button > "
				$(".btnp").empty()
				$(".btnp").append(str)

				$(".edit").click(function() {
					$.ajax({
						url: "editspesialisasi",
						type: "GET",
						dataType: "html",
						success: function(hasil) {
							$("#modalspesialisasi").modal('show')
							$(".isimodalspesialisasi").html(hasil)
						}
					})
					return false
				})
			}
		}
	})

})