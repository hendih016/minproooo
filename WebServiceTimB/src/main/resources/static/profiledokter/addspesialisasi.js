$(document).ready(function() {
	addspesialis();
	function addspesialis() {
		$.ajax({
			url: "http://localhost:81/api/specialization/list",
			type: "GET",
			success: function(hasil) {

				for (i = 0; i < hasil.length; i++) {
					let txt = ""
					txt = `<option value="${hasil[i].id}">${hasil[i].name}</option>`
					$("#iSpesialis").append(txt)
				}
			}
		})
	}

	$(".message").hide()

	$(".btn-add").click(function() {
		if ($('.name').val() == "--Pilih--") {
			$(".message").show()
			return false
		} else {
			var obj = {}
			obj.doctorId = sessionStorage.getItem("doctorid")
			obj.specializationId = $("#iSpesialis").val();
			var myjson = JSON.stringify(obj)
			$.ajax({
				url: "http://localhost:81/api/currentdoctor/add",
				type: "POST",
				contentType: "application/json",
				data: myjson,
				success: function(hasil) {
					console.log(hasil)
					/*alert(hasil)*/
					$("#modalspesialisasi").modal('hide')
					swal({
						title: "Success!",
						text: "anda berhasil menambahkan spesialisasi!",
						icon: "success",
						buttons: true,
					});
					kembali()
				}
			})
		}

	})

	$("#btnBatal").click(function() {
		$("#modalspesialisasi").modal('hide')
	})

	function kembali() {
		$.ajax({
			url: "http://localhost/spesialisasi",
			type: "GET",
			dataType: "html",
			success: function(hasil) {
				$(".isispesialisasi").html(hasil)
			}
		})
	}

})