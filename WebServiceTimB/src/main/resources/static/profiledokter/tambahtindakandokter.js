$(Document).ready(function() {
	let doctorid = sessionStorage.getItem("doctorid")

	$(".eror1").hide()
	$(".eror2").hide()

	$(".btnbatal").click(function() {
		$("#modaltindakandokter").modal('hide')
	})

	$(".btnsimpan").click(function() {
		let tdokter = $(".itindakan").val()
		let tindakanDokter = tdokter.toLowerCase()
		let valid = tdokter.replace(/ /g, "")

		if (valid.length == 0) {
			$(".eror1").show()
		} else {
			$(".eror1").hide()
			$.ajax({
				url: "http://localhost:81/api/tindakandokter/searchvalidasi/" + doctorid + "/" + tindakanDokter,
				type: "GET",
				contentType: "application/json",
				success: function(hasil) {
					console.log(hasil)
					if (hasil.length != 0) {
						$(".pesan1").show()
					} else {
						$(".pesan1").hide()
						var obj = {}
						obj.doctorid = doctorid
						obj.name = $(".inTindakan").val()
						var myJson = JSON.stringify(obj)
						$.ajax({
							url: "http://localhost:81/api/tindakandokter/add",
							type: "POST",
							contentType: "application/json",
							data: myJson,
							success: function(hasil) {
								console.log(hasil)
								alert("Tindakan berhasil disimpan")
								kembali()
							}
						})
					}
				}
			})
		}
	})

	function kembali() {
		$.ajax({
			url: "http://localhost/tindakandokter",
			type: "GET",
			datatype: "html",
			success: function(hasil) {
				$("#modaltindakandokter").modal('hide')
				$(".isimodaltindakandokter").html(hasil)
			}
		})
	}
})