$(document).ready(function() {
	let biodataid = sessionStorage.getItem("biodataid")

	$(".add").click(function() {
		$.ajax({
			url: "http://localhost/addalamat",
			type: "GET",
			datatype: "html",
			success: function(hasil) {
				$("#modaltabalamat").modal('show') //show modal terlebih dahulu
				$(".isimodaltabalamat").html(hasil)
			}
		})
		return false
	})

	$(".sorting").click(function() {
		let change = $(this)
		if (change.val() == "A-Z") {
			alert($(".sorting").val())
			change.val("Z-A");
		}
		else {
			alert($(".sorting").val())
			change.val("A-Z");
		}
		alert($(".sortby").val())
	})

	$(".sortby").append("<option class='lalamat' value='la'>Label Alamat</option>")
	$(".sortby").append("<option class='npenerima' value='na'>Nama Penerima</option>")

	function kembali() {
		$.ajax({
			url: "http://localhost/viewtabalamat",
			type: "GET",
			datatype: "html",
			success: function(hasil) {
				$("#modaltabalamat").modal('hide')
				$(".isimain").html(hasil)
			}
		})
	}

	$("#cari").on('input', function() {
		let cari = $(this).val()
		listtab(cari)
	})

	listtab()
	function listtab(isi) {
		let alamat = ""
		if (isi == undefined || isi == "") {
			alamat = "http://localhost:81/api/tabalamat/listalamat/" + biodataid
		} else {
			alamat = "http://localhost:81/api/tabalamat/search/" + biodataid + "/" + isi
		}
		$.ajax({
			url: alamat,
			type: "GET",
			success: function(hasil) {
				//console.log(hasil)
				let txt = ""
				for (i = 0; i < hasil.length; i++) {
					txt += "<tr>"
					txt += "<td><input class='text-primary' type = 'checkbox' id='checkitem" + hasil[i].id + "'></td>"
					txt += "<td><span class='text-primary'>" + hasil[i].label + "</span><br>"
					txt += hasil[i].recipient + ", "
					txt += hasil[i].recipientphonenumber + "<br>"
					txt += hasil[i].address + "</td>"
					txt += "<td><input type='button' value='Ubah' class='btn btn-warning btnupdate' name='" + hasil[i].id + "'> "
					txt += "<input type='button' value='Hapus' class='btn btn-danger btndelete' name='" + hasil[i].id + "' del='" + hasil[i].label + "'></td>"
					txt += "</tr>"
				}
				$(".tb1").empty()
				$(".tb1").append(txt)

				var cb = hasil

				$(".btnupdate").click(function() {
					let id = $(this).attr("name");
					sessionStorage.setItem("id", id)

					$.ajax({
						url: "http://localhost/updatealamat",
						type: "GET",
						datatype: "html",
						success: function(hasil) {
							$("#modaltabalamat").modal('show')
							$(".isimodaltabalamat").html(hasil)
						}
					})
					return false
				})

				$(".btndelete").click(function() {
					let id = $(this).attr("name");
					sessionStorage.setItem("id", id)

					$.ajax({
						url: "http://localhost/deletealamat",
						type: "GET",
						datatype: "html",
						success: function(hasil) {
							$("#modaltabalamat").modal('show')
							$(".isimodaltabalamat").html(hasil)
						}
					})
					return false
				})
			}
		})
	}


})