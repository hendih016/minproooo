//alert("haii")

$(document).ready(function() {

	$(".btnAdd").click(function() {
		$.ajax({
			url: "http://localhost/addhakakses",
			type: "GET",
			datatype: "html",
			success: function(hasil) {
				$("#ModalHakakses").modal('show')//modal show
				$(".isiModalHakakses").html(hasil)

			}
		})
		return false
	})

	function kembali() {
		$.ajax({
			url: "http://localhost/hakakses",
			type: "GET",
			datatype: "html",
			success: function(hasil) {
				$(".isiMain").html(hasil)
			}
		})
	}
	$("#inHakaksesCari").on('input', function() {
		let cari = $(this).val()
		list(cari)
	})

	list()

	function list(isi) {

		let alamat = ""
		if (isi == undefined || isi == "") {
			alamat = "http://localhost:81/api/role/list"
		} else {
			alamat = "http://localhost:81/api/role/listbynama/" + isi
		}
		$.ajax({
			url: alamat,
			type: "GET",
			success: function(hasil) {
				//alert("okeey")
				let txt = ""
				for (i = 0; i < hasil.length; i++) {
					txt += "<tr>"
					txt += "<td>" + (i + 1) + " </td>"
					txt += "<td>" + hasil[i].name + " </td>"
					txt += "<td>" + hasil[i].code + " </td>"
					txt += "<td><button class='btn btn-info btnUpdate' name='" + hasil[i].id + "'><i class='fa-solid fa-pen-to-square'></i>edit</button> <button class='btn btn-danger btnDelete' name='" + hasil[i].id + "' hps='" + hasil[i].name + "'><i class='fa-solid fa-trash-can'></i> hapus</button> </td>"
					txt += "</tr>"
				}
				$(".tb1").empty()
				$(".tb1").append(txt)
				$(".btnUpdate").click(function() {
					let id = $(this).attr("name");
					sessionStorage.setItem("id", id)
					$.ajax({
						url: "http://localhost/updatehakakses",
						type: "GET",
						datatype: "html",
						success: function(hasil) {
							$("#ModalHakakses").modal('show')
							$(".isiModalHakakses").html(hasil)
						}
					})
					return false

					//alert("hapus")

					//window.open("/updateproduct")
				})
				$(".btnDelete").click(function() {
					let id = $(this).attr("name");
					sessionStorage.setItem("id", id)
					let name = $(this).attr("hps");
					sessionStorage.setItem("name", name)
					swal({
						title: "apakah anda yakin?",
						text: `Data ${name} akan dihapus`,
						icon: "warning",
						buttons: true,
						dangerMode: true,
					})
						.then((willDelete) => {
							if (willDelete) {
								let id = sessionStorage.getItem("id") //kalau diclear maka session storage nya hilang
								var obj = {}
								obj.id = id
								obj.name = $(".Inm").val()
								obj.code = $("#iCd").val()
								var myJson = JSON.stringify(obj)
								$.ajax({
									url: "http://localhost:81/api/role/delete",
									type: "DELETE",
									contentType: "application/json",
									data: myJson,
									success: function(hasil) {
										swal({
											title: "Success!",
											text: "Delete Success!",
											icon: "success",
											buttons: true,
										});
										kembali()
										//window.open("/product")

									}

								})
							} else {

							}
						});


					//	window.open("/delete")
				})
				//$(".tb1").append("<tr> <td>1</td> <td>2</td> <td>3</td> </tr>")
			}

		})
	}
	$(".btnKembali").click(function() {
		$.ajax({
			url: "http://localhost/hakakses",
			type: "GET",
			datatype: "html",
			success: function(hasil) {
				$("#ModalHakakses").modal('hide')
			}
		})
		return false
	})

	$(".btnRefresh").click(function() {
		list()
	})
})