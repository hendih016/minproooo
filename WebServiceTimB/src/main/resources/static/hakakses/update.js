

hakaksesById() //untuk memanggil function

//buat function untuk get by id
function hakaksesById(){
	let id=sessionStorage.getItem("id") //kalau diclear maka session storage nya hilang
	$.ajax({
		url			: "http://localhost:81/api/role/list/"+id,
		type		: "GET",
		success		: function(hasil){
			$("#iId").val(hasil.id)
			$("#iNm").val(hasil.name)
			$("#iCd").val(hasil.code)
		}
		
	})
}

$(document).ready(function(){
	function kembali(){
		$.ajax({
			url		: "http://localhost/hakakses",
			type	: "GET",
			datatype: "html",
			success	: function(hasil){
				$(".isiMain").html(hasil)
			}
		})
	}
	$(".btnKembali").click(function(){
		$.ajax({
			url		: "http://localhost/hakakses",
			type	: "GET",
			datatype: "html",
			success	: function(hasil){
				$("#ModalHakakses").modal('hide')
			}
		})
		return false
	})
	
	$(".bt-update").click(function(){
		let id=sessionStorage.getItem("id") //kalau diclear maka session storage nya hilang
		var obj={}
		obj.id=id
		obj.name=$("#iNm").val()
		obj.code=$("#iCd").val()
		var myJson = JSON.stringify(obj)
		$.ajax({
			url  		: "http://localhost:81/api/role/update",
			type        : "PUT",
			contentType : "application/json",
			data 		: myJson,
			success		: function(hasil){
				$("#ModalHakakses").modal('hide')
				swal({
				  title: "Success!",
				  text: "Update Success!",
				  icon: "success",
				  buttons: true,
				});
				kembali()
				 //window.open("/product")
				
				}
				
		})
	})
})