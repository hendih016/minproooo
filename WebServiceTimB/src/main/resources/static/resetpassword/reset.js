$(document).ready(function(){
	
	$("#emailHelp").hide()
	$("#emailemptyHelp").hide()
	$("#emailwrongHelp").hide()
	$("TokenHelp").hide()
	$("TokenemptyHelp").hide()
//validasi penulisan email
	function IsEmail(email) {
  		var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  		if(!regex.test(email)) {
    		return false;
  		}else{
    		return true;
  		}
  	}
	
	//check email
	let email = ""
	let passlama = ""
	$("#btnemail").click(function(){
		email = $("#formEmail").val();
		if(email==""){
				$("#emailemptyHelp").show()
				$("#emailHelp").hide()
				$("#emailwrongHelp").hide()
		}else{
			var mail=IsEmail(email)
			if(mail==false){
				$("#emailHelp").hide()
				$("#emailemptyHelp").hide()
				$("#emailwrongHelp").show()
			}else{
				sessionStorage.setItem("formEmail", email)	
				$.ajax({
					url		: "http://localhost:81/api/user/cekemail/"+email,
					type	: "GET",
					success	: function(hasil){
						if(hasil==0){
							$("#emailHelp").show()
							$("#emailemptyHelp").hide()
							$("#emailwrongHelp").hide()
						}else{
				
							buattoken(email)
							
						}
					}
				})
			}
		}
		
		$.ajax({
		url		: "http://localhost:81/api/user/cekpassold/"+email,
		type	: "GET",
		success	: function(hasil){
			passlama=hasil;
			sessionStorage.setItem("oldPassword", passlama)		
						
		}
	})
})
	

	function buattoken(email){
		newtoken()
	}

	function newtoken(){
	
		//buat token
		var obj={}
		obj.email = email
		var myJson = JSON.stringify(obj)
		$.ajax({
			url			: "http://localhost:81/api/token/post",
			type		: "POST",
			contentType	: "application/json",
			data		: myJson,
			success		: function(hasil){
				getoken(email)
			}
		})
		
}

	function getoken(email) {
		$.ajax({
			url: "http://localhost:81/api/token/gettoken/" + email,
			type: "GET",
			success: function(hasil) {
				sessionStorage.setItem("idtoken", hasil)
				window.location.href = "/veritifikasipass" 
			}
		})
	}
	
	$("#btnotp").click(function(){
	let token= $("#itoken").val()
	let idtoken = sessionStorage.getItem("idtoken")
	cekvalid(token)
	})

	$("#btnresendotp").hide()
	$("#returntoemailver").hide()
	

function cekvalid(token){
	let idtoken = sessionStorage.getItem("idtoken")
	$.ajax({
				url		: "http://localhost:81/api/token/cekvalid/"+idtoken,
				type	: "GET",
				success	: function(hasil){
					console.log(idtoken)
					if(hasil<=0){
					$("TokenHelp").show()
					$("TokenemptyHelp").hide()
					}else{
						cektoken(token)
					}
				}
			})
}

startcountdown()
function startcountdown(){
	const intervalID =  setInterval(loopcount,1000)
	
	$("#returntoemailver").click(function(){
		window.location.href = "/resetpassword" 
		})
		
		//kirim ulang otp
		$("#btnresendotp").click(function(){
			minute = 0
			second = 60
			//clearInterval(intervalID)
			$("#count").show()
		})
		
		
		var minute = 0
 		var second = 60
 		
 		function loopcount(){
			
	 		if(minute==0 && second == 1){
				 $("#returntoemailver").show()
				 $("#btnresendotp").show()
				 $("#count").hide()
				//clearInterval(intervalID) 
		 		//document.getElementById("count").innerHTML = "00:00";
	 		}else{
		 		second--;
		 		if(second==0){
			 		minute--;
			 		second=60;
			 		if(minute==0){
				 		minute=minute;
			 		}
		 		}
		 		if(minute.toString().length == 1){
					 minute = "0"+minute;
				 }
				 if(second.toString().length == 1){
					 second = "0"+second;
				 }
		 		document.getElementById("count").innerHTML = "Kirim ulang kode OTP dalam "+minute+":"+second;
	 		}
 		}
	}	
	
	function deactiveotplama(){
		let idtoken = sessionStorage.getItem("idtoken")
		let email	= sessionStorage.getItem("email")
		$.ajax({
			url			: "http://localhost:81/api/token/setexpired/"+idtoken,
			type		: "PUT",
			success		: function(hasil){
			}
		})
	}
	
	//kirim ulang otp
		$("#btnresendotp").click(function(){
			let email	= sessionStorage.getItem("email")
			deactiveotplama()
			newtoken2(email)
			
		})
		
	function newtoken2(){
		let email	= sessionStorage.getItem("email")
		//buat token
		var obj={}
		obj.email = email
		var myJson = JSON.stringify(obj)
		$.ajax({
			url			: "http://localhost:81/api/token/post",
			type		: "POST",
			contentType	: "application/json",
			data		: myJson,
			success		: function(hasil){
				getoken(email)
			}
		})
		
}
		
		function cektoken(token){
		let idtoken = sessionStorage.getItem("idtoken")
		$.ajax({
				url		: "http://localhost:81/api/token/cektoken/"+idtoken+"/"+token,
				type	: "GET",
				success	: function(hasil){
					console.log(hasil)
					if(hasil<=0){
						alert("Token tidak sesuai")
						
					}else{
						window.location.href = "/newpass" 
					}
				}
			})
	}
	
	let password = "";
	let password2 = "";
	$("#passHelp").hide()
	$("#passemptyHelp").hide()
	$("#passwrongHelp").hide()
	$("#pass2wrongHelp").hide()
	
	$("#setPass").click(function(){
		let passbaru = "";
		password = $("#iPass1").val();
		password2 = $("#iPass2").val();
		cekpassword(password, password2)
		
	})
		var lowerCaseLetters = /[a-z]/g;
		var upperCaseLetters = /[A-Z]/g;
		var numbers = /[0-9]/g;
		var numBoolean=false
    	var specialChars = "!@#$%^&*()-_=+[{]}\\|;:'\",<.>/?`~";
    	
	function cekpassword(password, password2){
		let email = sessionStorage.getItem("formEmail")
	

		for (let i = 0; i < specialChars.length; i++) {
            for (let j = 0; j < password.length; j++) {
                if (specialChars[i] == password[j]) {
                    numBoolean = true;
                }
            }
        }
        if(password.length==0){
			$("#passemptyHelp").show()
			$("#passwrongHelp").hide()
			$("#pass2wrongHelp").hide()
			$("#passHelp").hide()
		}else if(!(password.match(lowerCaseLetters))>0 || !(password.match(upperCaseLetters))>0 || !(password.match(numbers))>0 || numBoolean==false || password.length<8){
			$("#passHelp").show()
			$("#passemptyHelp").hide()
			$("#passwrongHelp").hide()
			$("#pass2wrongHelp").hide()
		}else if(password2.length==0){
			$("#passwrongHelp").show()
			$("#pass2wrongHelp").hide()
			$("#passHelp").show()
			$("#passemptyHelp").hide()
		}else if(password!=password2){
			$("#pass2wrongHelp").show()
			$("#passHelp").show()
			$("#passemptyHelp").hide()
			$("#passwrongHelp").hide()
		}else{
			$.ajax({
					url		: "http://localhost:81/api/user/cekpass/"+password+"/"+email,
					type	: "GET",
					success	: function(hasil){
						if(hasil > 0){
							alert("password sama")
						}else{
							var passbaru =  password;
							createpassbaru(passbaru)
							
						}
					}
				})
		}
	}
	
	 
	function createpassbaru(passbaru){
		var obj={}
		let oldpassword= sessionStorage.getItem("oldPassword")
		obj.oldpassword= oldpassword
		obj.newpassword = passbaru
		var myJson = JSON.stringify(obj)
		$.ajax({
			url			: "http://localhost:81/api/resetpassword/post",
			type		: "POST",
			contentType	: "application/json",
			data		: myJson,
			success		: function(hasil){
				createupdatepassword(passbaru)
			}
		})
	}
	
	function createupdatepassword(passbaru){
		 var obj={}
		 let email= sessionStorage.getItem("formEmail")
		 obj.email = email
		 obj.password=passbaru
		 var myJson = JSON.stringify(obj)
		 $.ajax({
			 url		 :"http://localhost:81/api/user/setPasswordbaru/"+email+"/"+password,
			 type		 :"PUT",
			 contentType :"application/json",
			 data		 : myJson,
			 success     : function(hasil){
					window.location.href = "/" 
			 }
		 })
		 
	}
	
	
	
})