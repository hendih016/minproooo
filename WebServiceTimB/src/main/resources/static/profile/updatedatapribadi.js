$(document).ready(function() {
	$("#namaHelp").hide()
	$("#noHelp").hide()

	let biodataid = sessionStorage.getItem("biodataid")

	tampil()
	function tampil() {
		$.ajax({
			url: "http://localhost:81/api/tabprofile/listprofile/" + biodataid,
			type: "GET",
			success: function(hasil) {
				alert(hasil[0].fullname)
				$(".inamalengkap").val(hasil[0].fullname)
				$(".itanggallahir").val(hasil[0].dob)
				let hp = hasil[0].mphone
				$(".inomorhp").val(hp)
			}
		})
	}

	var namauser = $(".inamalengkap").val()
	var nohp = $(".inomorhp").val()
	console.log(namauser)
	$(".bt-update").click(function() {
		if (namauser == "") {
			$("#namaHelp").show()
			$("#noHelp").hide()
		} else if (nohp.length == 0) {
			$("#noHelp").show()
			$("#namaHelp").hide()
		} else if (namauser == "" && nohp.length == 0) {
			$("#namaHelp").show()
			$("#noHelp").show()
		} else {
			var obj = {}
			obj.biodata_id = sessionStorage.getItem("biodataid")
			obj.fullname = $(".inamalengkap").val()
			obj.dob = $(".itanggallahir").val()
			obj.mobile_phone = $(".inomorhp").val()
			var myJson = JSON.stringify(obj)
			$.ajax({
				url: "http://localhost:81/api/tabprofile/editdatapribadi",
				type: "PUT",
				contentType: "application/json",
				data: myJson,
				success: function(hasil) {
					$("#ModalProfile").modal('hide')
					swal({
						title: "Success!",
						text: "Update data pribadi anda berhasil!",
						icon: "success",
						buttons: true,
					});
					kembali()
				}
			})

		}
	})

	function kembali() {
		$.ajax({
			url: "profile",
			type: "GET",
			datatype: "html",
			success: function(hasil) {
				$(".isiMain").html(hasil)
			}
		})
		return false
	}

})