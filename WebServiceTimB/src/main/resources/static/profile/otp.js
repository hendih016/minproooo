$(document).ready(function() {
	$("#otpHelp").hide()
	$("#otpspoilHelp").hide()
	$("#btnresendotp").hide()
	$("#returntoemailver").hide()

	//starting countdown to resend otp
biodata()
	function biodata() {
		let idbiodata = sessionStorage.getItem("biodataid")
		$.ajax({
			url: "http://localhost:81/api/user/cekbiodataid/" + idbiodata,
			type: "GET",
			success: function(hasil) {
				let iduser=hasil.id
				sessionStorage.setItem("iduser",iduser)
				console.log("iduser",iduser)
			}
		})
	}

	startcountdown()
	function startcountdown() {
		var minute = 0
		var second = 60

		const intervalID = setInterval(loopcount, 1000)
		//gunakan email lain
		$("#returntoemailver").click(function() {
			clearInterval(intervalID)
			$("#modalotp").modal('hide')
			$("#modalemail").modal('show')
		})

		$("#btnx").click(function() {
			minute = 0
			second = 60
			clearInterval(intervalID)
		})


		$("#btnotp").click(function() {
			minute = 0
			second = 60
			//clearInterval(intervalID)
			$("#count").show()
		})

		$("#btnresendotp").click(function() {
			minute = 0
			second = 60
			clearInterval(intervalID)
			$("#btnresendotp").hide()
			$("#count").show()
			startcountdown()
		})

		function loopcount() {

			if (minute == 0 && second == 1) {
				$("#returntoemailver").show()
				$("#btnresendotp").show()
				$("#count").hide()
				clearInterval(intervalID)
				//document.getElementById("count").innerHTML = "00:00";
			} else {
				second--;
				if (second == 0) {
					minute--;
					second = 60;
					if (minute == 0) {
						minute = minute;
					}
				}
				if (minute.toString().length == 1) {
					minute = "0" + minute;
				}
				if (second.toString().length == 1) {
					second = "0" + second;
				}
				document.getElementById("count").innerHTML = "Kirim ulang kode OTP dalam " + minute + ":" + second;
			}
		}

	}

	$("#btnacceptotp").click(function() {
		$("#otpspoilHelp").hide()
		$("#otpHelp").hide()

		let token = $("#iotp").val()
		cekvalid(token)
	})

	function cekvalid(token) {
		let idtoken = sessionStorage.getItem("idtoken")
		$.ajax({
			url: "http://localhost:81/api/token/cekvalid/" + idtoken,
			type: "GET",
			success: function(hasil) {
				if (hasil <= 0) {
					$("#otpspoilHelp").show()
				} else {
					cektoken(token, idtoken)
				}
			}
		})
	}

	function cektoken(token, idtoken) {
		$.ajax({
			url: "http://localhost:81/api/token/cektoken/" + idtoken + "/" + token,
			type: "GET",
			success: function(hasil) {
				if (hasil <= 0) {
					$("#otpHelp").show()

				} else {
					var obj = {}
					obj.id=sessionStorage.getItem("iduser")
					obj.biodataid = sessionStorage.getItem("biodataid")
					obj.email = sessionStorage.getItem("email")
					var myJson = JSON.stringify(obj)
					console.log(myJson)
					$.ajax({
						url: "http://localhost:81/api/user/updateemail",
						type: "PUT",
						contentType: "application/json",
						data: myJson,
						success: function(hasil) {
							//$("#modalotp").modal('hide')
							alert("berhasil merubah email")
							window.location.href = "logout"
						}
					})
					/*$("#modalpassword").modal('show')*/
				}
			}
		})
	}
function kembali() {
		$.ajax({
			url: "profile",
			type: "GET",
			datatype: "html",
			success: function(hasil) {
				$(".isiMain").html(hasil)
			}
		})
		return false
	}
	//kirim ulang otp
	$("#btnresendotp").click(function() {
		deactiveotplama()
	})

	function deactiveotplama() {
		let email = sessionStorage.getItem("email")
		$.ajax({
			url: "http://localhost:81/api/token/setexpired/" + email,
			type: "PUT",
			success: function(hasil) {
				let email = sessionStorage.getItem("email")
				buattoken(email)
			}
		})
	}

	function buattoken(email) {
		//buat token
		var obj = {}
		obj.email = email
		var myJson = JSON.stringify(obj)
		$.ajax({
			url: "http://localhost:81/api/token/post",
			type: "POST",
			contentType: "application/json",
			data: myJson,
			success: function(hasil) {
				gettoken(email)
			}
		})
	}

	var idtoken = 0;
	function gettoken(email) {
		$.ajax({
			url: "http://localhost:81/api/token/gettoken/" + email,
			type: "GET",
			success: function(hasil) {
				idtoken = hasil
				sessionStorage.setItem("idtoken", idtoken)
				sessionStorage.setItem("email", email)
				$("#btnresendotp").hide()
			}
		})
	}

	$("#btnx").click(function() {
		let email = sessionStorage.getItem("email")
		$.ajax({
			url: "http://localhost:81/api/token/setexpired/" + email,
			type: "PUT",
			success: function(hasil) {
			}
		})
	})
})
