$(document).ready(function() {
	var biodataid1 = sessionStorage.getItem("biodataid")

	tampilsesuaibiodata()
	function tampilsesuaibiodata() {

		$.ajax({
			url: "http://localhost:81/api/user/cekbiodataid/" + biodataid1,
			type: "GET",
			success: function(hasil) {
				var emailuser = hasil.email
				sessionStorage.setItem("emailuser", emailuser)
			}
		})
	}

	$("#pswHelp").hide()
	$("#pswemptyHelp").hide()
	$("#pswreHelp").hide()
	$("#pswreemptyHelp").hide()
	$("#hide_eyere").hide()
	$("#hide_eye").hide()

	$("#show_eye").click(function() {
		$("#hide_eye").show()
		$("#show_eye").hide()
		$("#ipswd").attr("type", "text")
	})

	$("#hide_eye").click(function() {
		$("#show_eye").show()
		$("#hide_eye").hide()
		$("#ipswd").attr("type", "password")
	})

	$("#show_eyere").click(function() {
		$("#hide_eyere").show()
		$("#show_eyere").hide()
		$("#irepswd").attr("type", "text")
	})

	$("#hide_eyere").click(function() {
		$("#show_eyere").show()
		$("#hide_eyere").hide()
		$("#irepswd").attr("type", "password")
	})


	let password = "";
	let password2 = "";
	$("#btnacceptpsw").click(function() {
		$("#pswHelp").hide()
		$("#pswemptyHelp").hide()
		$("#pswreHelp").hide()
		$("#pswreemptyHelp").hide()

		password = $("#ipswd").val();
		password2 = $("#irepswd").val();
		cekpassword(password, password2)
	})


	var lowerCaseLetters = /[a-z]/g;
	var upperCaseLetters = /[A-Z]/g;
	var numbers = /[0-9]/g;
	var numBoolean = false
	var specialChars = "!@#$%^&*()-_=+[{]}\\|;:'\",<.>/?`~";

	function cekpassword(password, password2) {
		for (let i = 0; i < specialChars.length; i++) {
			for (let j = 0; j < password.length; j++) {
				if (specialChars[i] == password[j]) {
					numBoolean = true;
				}
			}
		}
		if (password.length == 0) {
			$("#pswemptyHelp").show()
		} else if (!(password.match(lowerCaseLetters)) > 0 || !(password.match(upperCaseLetters)) > 0 || !(password.match(numbers)) > 0 || numBoolean == false || password.length < 8) {
			$("#pswHelp").show()
		} else if (password2.length == 0) {
			$("#pswreemptyHelp").show()
		} else if (password != password2) {
			$("#pswreHelp").show()
		} else {
			var em = sessionStorage.getItem("emailuser")
			sessionStorage.setItem("password1", password)
			$.ajax({
				url: "http://localhost:81/api/user/cekpass/" + password + "/" + em,
				type: "GET",
				success: function(hasil) {
					if (hasil > 0) {
						$("#ModalProfile").modal('hide')
						$.ajax({
							url: "setpasswordnew",
							type: "GET",
							dataType: "html",
							success: function(hasil) {
								$(".content4").html(hasil)
							}
						})
						$("#modalpassword2").modal('show')
						//alert("sama")
					} else {
						swal({
							title: "failed!",
							text: "Password Tidak Sesuai!",
							icon: "error",
							buttons: true,
						});

					}
				}
			})

			//$("#modaldaftar").modal('show')
		}
	}

})

