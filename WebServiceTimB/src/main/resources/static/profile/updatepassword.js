$(document).ready(function() {
	var biodataid1 = sessionStorage.getItem("biodataid")

	tampilsesuaibiodata()
	function tampilsesuaibiodata() {

		$.ajax({
			url: "http://localhost:81/api/user/cekbiodataid/" + biodataid1,
			type: "GET",
			success: function(hasil) {
				var passworduser = hasil.password
				sessionStorage.setItem("passworduser", passworduser)
			}
		})
	}


	let password = "";
	let password2 = "";
	$("#passHelp").hide()
	$("#passHelp2").hide()
	$("#passwrong2Help").hide()
	$("#pass2wrongHelp").hide()
	$("#pass2wrongHelp3").hide()
	$("#hide_eyes1").hide()
	$("#hide_eye1").hide()


	$("#setPass").click(function() {
		let passbaru = "";
		password = $("#iPass1").val();
		password2 = $("#iPass2").val();
		cekpassword(password, password2)

	})

	$("#show_eye1").click(function() {
		$("#hide_eye1").show()
		$("#show_eye1").hide()
		$("#iPass1").attr("type", "text")
	})

	$("#hide_eye1").click(function() {
		$("#show_eye1").show()
		$("#hide_eye1").hide()
		$("#iPass1").attr("type", "password")
	})

	$("#show_eyes1").click(function() {
		$("#hide_eyes1").show()
		$("#show_eyes1").hide()
		$("#iPass2").attr("type", "text")
	})

	$("#hide_eyes1").click(function() {
		$("#show_eyes1").show()
		$("#hide_eyes1").hide()
		$("#iPass2").attr("type", "password")
	})

	var lowerCaseLetters = /[a-z]/g;
	var upperCaseLetters = /[A-Z]/g;
	var numbers = /[0-9]/g;
	var numBoolean = false
	var specialChars = "!@#$%^&*()-_=+[{]}\\|;:'\",<.>/?`~";

	function cekpassword(password, password2) {
		var email = sessionStorage.getItem("emailuser")

		for (let i = 0; i < specialChars.length; i++) {
			for (let j = 0; j < password.length; j++) {
				if (specialChars[i] == password[j]) {
					numBoolean = true;
				}
			}
		}
		if (password.length == 0) {
			$("#passHelp2").show()
			$("#passwrong2Help").hide()
			$("#pass2wrongHelp").hide()
			$("#passHelp").hide()
		} else if (!(password.match(lowerCaseLetters)) > 0 || !(password.match(upperCaseLetters)) > 0 || !(password.match(numbers)) > 0 || numBoolean == false || password.length < 8) {
			$("#passHelp").show()
			$("#passHelp2").hide()
			$("#passwrong2Help").hide()
			$("#pass2wrongHelp").hide()
		} else if (password2.length == 0) {
			$("#passwrong2Help").show()
			$("#pass2wrongHelp").hide()
			$("#passHelp").hide()
			$("#passHelp2").hide()
		} else if (password != password2) {
			$("#pass2wrongHelp").show()
			$("#passHelp").hide()
			$("#passHelp2").hide()
			$("#passwrong2Help").hide()
		} else {
			//cek password lama
			$.ajax({
				url: "http://localhost:81/api/user/cekpass/" + password + "/" + email,
				type: "GET",
				success: function(hasil) {
					if (hasil > 0) {
						$("#pass2wrongHelp3").show()
					} else {
						var passbaru = password;
						createpassbaru(passbaru)

					}
				}
			})
		}
	}


	function createpassbaru(passbaru) {
		var obj = {}
		let oldpassword = sessionStorage.getItem("passworduser")
		obj.oldpassword = oldpassword
		obj.newpassword = passbaru
		var myJson = JSON.stringify(obj)
		$.ajax({
			url: "http://localhost:81/api/resetpassword/post",
			type: "POST",
			contentType: "application/json",
			data: myJson,
			success: function(hasil) {
				createupdatepassword(passbaru)
			}
		})
	}

	function createupdatepassword(passbaru) {
		var obj = {}
		let email = sessionStorage.getItem("emailuser")
		obj.email = email
		obj.password = passbaru
		var myJson = JSON.stringify(obj)
		$.ajax({
			url: "http://localhost:81/api/user/setPasswordbaru/" + email + "/" + password,
			type: "PUT",
			contentType: "application/json",
			data: myJson,
			success: function(hasil) {
				alert("password berhasil diubah")
				window.location.href = "logout"
			}
		})

	}
})

