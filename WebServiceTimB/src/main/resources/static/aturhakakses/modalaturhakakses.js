$(document).ready(function() {
	let roleid= sessionStorage.getItem("roleid")

	$(".btnbatal").click(function() {
		$("#ModalAturHakAkses").modal("hide")
	})

	$(".sAksesmenu").click(function() {
		if ($('.sAksesmenu').is(':checked') == true) {
			$('.aksesmenu').each(function() {
				if ($('.aksesmenu').is(':checked') == false) {
					$(".aksesmenu").attr("checked", true)
				}
			})
		} else {
			$('.aksesmenu').each(function() {
				if ($('.aksesmenu').is(':checked') == true) {
					$(".aksesmenu").attr("checked", false)
				}
			})
		}
	})

	function list() {
		$.ajax({
			url: "http://localhost:81/api/menurole/listmenu1",
			type: "GET",
			success: function(hasil) {
				console.log(hasil)
				let txt = ""
				for (i = 0; i < hasil.length; i++) {
					txt += "<tr>"
					txt += "<td><input type='checkbox' class='c"+hasil[i].id+"'>" + hasil[i].name + "</td>"
					txt += "</tr>"
				}
				$(".menurole").append(txt)
				cek()
			}
		})
	}
	list()
	
	function cek(){
		$.ajax({
			url: "http://localhost:81/api/menurole/listmenu1/"+roleid,
			type: "GET",
			success: function(hasil) {
				console.log(hasil)
				for (i = 0; i < hasil.length; i++) {
					$(".c"+hasil[i].id).attr("checked",true)
				}
			}
		})
	}

})