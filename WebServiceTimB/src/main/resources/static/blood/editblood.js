$(document).ready(function() {

	BloodById()
	$("#kodecheck").hide();
	function BloodById() {
		let id = sessionStorage.getItem("id")
		$.ajax({
			url: "http://localhost:81/api/blood/list/" + id,
			type: "GET",
			success: function(hasil) {
				$("#iId").val(hasil.id)
				$("#iKode").val(hasil.code)
				$("#iDes").val(hasil.description)
			}
		})
	}

	$("#btnsave").click(function() {
		let id = sessionStorage.getItem("id")
		let kode = $("#iKode").val()
		if (kode == "" || kode.length == 0 || kode == undefined) {
			$("#kodecheck").show();
			return false
		}
		else {
			$.ajax({
				url: "http://localhost:81/api/blood/listbyname/" + kode,
				type: "GET",
				contentType: "application/json",
				success: function(hasil) {
					if (hasil.length != 0) {
						alert("data sudah ada")
					}
					else {
						var obj = {}
						obj.id = id
						obj.code = $("#iKode").val()
						obj.description = $("#iDes").val()

						var myJson = JSON.stringify(obj)

						$.ajax({

							url: "http://localhost:81/api/blood/edit",
							type: "PUT",
							contentType: "application/json",
							data: myJson,
							success: function(hasil) {
								swal({
									title: "Succes?",
									text: "Data berhasil di edit",
									icon: "success",
									buttons: true,
								})
								$("#modalBlood").modal('hide')
								kembali()
							}
						})
					}
				}
			})
		}


	})

	function kembali() {
		$.ajax({
			url: "http://localhost/blood",
			type: "GET",
			dataType: "html",
			success: function(hasil) {
				$(".isiMain").html(hasil)
			}
		})
	}

})