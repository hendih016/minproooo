populateSelect()

function populateSelect() {

	let getspecId, getnameTxt, getlocId, getmedtId

	$.ajax({
		url: "http://localhost:81/api/doctor/poplocs",
		type: "GET",
		success: function(hasil) {
			for (var i = 0; i < hasil.length; i++) {
				$('#sbLok').append($('<option>', {
					text: hasil[i].namalokasi,
					name: hasil[i].id
				}));
			}
		}
	})

	$.ajax({
		url: "http://localhost:81/api/doctor/popspec/",
		type: "GET",
		success: function(hasil) {
			for (var i = 0; i < hasil.length; i++) {
				$('#sbSpec').append($('<option>', {
					text: hasil[i].name,
					name: hasil[i].specialisasi
				}));
			}
		}
	})

	$('#sbSpec').change(function() {
		$('#sbMed').empty()

		let urlpopmedt = "http://localhost:81/api/doctor/popmedt/"

		getspecId = $('#sbSpec').find(":selected").attr("name")

		if (getspecId != undefined) {
			urlpopmedt = urlpopmedt + getspecId
			$.ajax({
				url: urlpopmedt,
				type: "GET",
				success: function(hasil) {
					$('#sbMed').append($('<option>', {
						text: " ",
						name: 0
					}));

					for (var i = 0; i < hasil.length; i++) {
						$('#sbMed').append($('<option>', {
							text: hasil[i].name,
							name: hasil[i].specialization_id,
							class: "spOption"
						}));
					}
				}
			})
		}
	})

	$('#btnCariDokter').click(function() {
		if (getspecId == 0 || getspecId == undefined) {
			alert("Specialisasi tidak boleh kosong")
		} else {
			getlocId = $('#sbLok').find(":selected").attr("name")
			getmedtId = $('#sbMed').find(":selected").attr("name")
			getnameTxt = $('#iNmDoc').val()

			url = "http://localhost:81/api/doctor/caridoctor?specId=" + getspecId + "&name=%20" + getnameTxt + "&medtId=" + getmedtId + "&locId=" + getlocId

			$.ajax({
				url: url,
				type: "GET",
				success: function(hasilcaridokter) {
					let cardDokter = ""
					//					console.log("Semua " + hasilcaridokter)

					$.each(hasilcaridokter, function(key, value) {
						let cardAlamat = ""
						url = "http://localhost:81/api/doctor/listlocation?idDoctor=" + value.id
						$.ajax({
							url: url,
							type: "GET",
							success: function(hasillistlokasi) {
								$.each(hasillistlokasi, function(key, value) {
									cardAlamat += "<h6 class='card-subtitle text-muted'>" + value.location + "</h6>"
								})
								//								console.log("Alamat " + cardAlamat)
							}
						})

						url = "http://localhost:81/api/doctor/listdatadoctor?idDoctor=" + value.id
						$.ajax({
							url: url,
							type: "GET",
							success: function(hasillistdatadoctor) {
								$.each(hasillistdatadoctor, function(key, value) {
									cardDokter += "<div class='col-12 col-lg-6'>"
									cardDokter += "<div class=' card flex-fill w-100'>"
									cardDokter += "<div class='row'>"
									cardDokter += "<div class='col-10 col-sm-8'>"
									cardDokter += "<div class='card-header'>"
									cardDokter += "<h2 class='card-title text-primary text-uppercase d-inline-block'>" + value.fullname + "</h2>"
									cardDokter += "<h6 class='card-subtitle text-muted mb-2'>" + value.spesialisasi + "</h6>"
									cardDokter += "<h6 class='card-subtitle text-muted mb-2'>" + value.pengalaman + " tahun pengalaman</h6>"
									cardDokter += "</div>"
									cardDokter += "<div class='card-body'>"
									cardDokter += cardAlamat
									cardDokter += "</div>"
									cardDokter += "<div class='text-center'>"
									cardDokter += "<input class='btn btn-primary text-center btn-lihat' idDokter=" + value.id + " type='button' value='Lihat Selengkapnya'>"
									cardDokter += "</div>"
									cardDokter += "</div>"
									cardDokter += "<div class='col-10 col-sm-4 text-center mt-2 mb-2'>"
									cardDokter += "<div class='card-body'>"
									cardDokter += "<img src='img/avatars/avatar-4.jpg' alt='Christina Mason'"
									cardDokter += "class='img-fluid rounded-circle mb-2' width='128' height='128'>"
									cardDokter += "</div>"
									cardDokter += "<input class='btn btn-primary mb-2 btn-chat' idDokter=" + value.id + " type='button' value='Chat'>"
									cardDokter += "<input class='btn btn-primary mb-2 btn-janji' idDokter=" + value.id + " type='button' value='Buat Janji'>"
									cardDokter += "</div>"
									cardDokter += "</div>"
									cardDokter += "</div>"
									cardDokter += "</div>"
								})
								$(".isiMain").html(cardDokter)

								//button nya copy dari ini aja jal, tinggal ganti url
								$(".btn-lihat").click(function() {
									let iddetaildokter = $(this).attr("idDokter")
									url = "http://localhost/detaildokter"
									$.ajax({
										url: url,
										type: "GET",
										success: function(hasildetaildokter) {
											sessionStorage.setItem("idDokter", iddetaildokter)
											$(".isiMain").html(hasildetaildokter)
										}
									})
								})
								//=================================

							}
						})
					})
					/*					console.log(cardDokter)
										$('.isihasil').html(cardDokter)*/
				}
			})
		}

		$("#wadahModal").modal('hide')
	})

	$(".btnmenu1").click(function() {
		let alamat = $(this).attr("jump")
		$.ajax({
			url: alamat,
			type: "GET",
			datatype: "html",
			success: function(hasil) {
				$(".isiMain").html(hasil)
			}
		})
		return false
	})
}