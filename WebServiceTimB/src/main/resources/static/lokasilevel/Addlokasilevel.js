
levlok()

function levlok() {

	apibaseserver = "http://localhost:81/"
	apibaseurl = "api/levellokasi/"

	let listurl = apibaseserver + apibaseurl

	$("#aName").focusout(function() {
		$.ajax({
			url: listurl + "cekduploclev/" + $("#aName").val(),
			type: "GET",
			success: function(hasil) {
				if (hasil === "") {
					$("#btnSimpan").prop('disabled', false);
				} else {
					alert(hasil + " sudah ada")
					$("#btnSimpan").prop('disabled', true);
				}
			}
		});
	});

	$("#btnSimpan").click(function() {

		if ($("#aName").val() == "") {
			alert("Nama tidak boleh kosong!!")
		} else {
			var obj = {}
			obj.name = $("#aName").val()
			obj.abbr = $("#aAbbr").val()
			obj.c_by = 1
			var myJson = JSON.stringify(obj)

			$.ajax({
				url: "http://localhost:81/api/levellokasi/add",
				type: "POST",
				contentType: "application/json",
				data: myJson,
				success: function(hasil) {
					console.log(hasil)
					alert("Tambah berhasil")
					$("#modalVariants").modal('hide')
					location.reload()
				}
			});
		}
	});
}