(function($) {
	"use strict";

	// Dropdown on mouse hover
	$(document).ready(function() {
		

		
		function toggleNavbarMethod() {
			if ($(window).width() > 992) {
				$('.navbar .dropdown').on('mouseover', function() {
					$('.dropdown-toggle', this).trigger('click');
				}).on('mouseout', function() {
					$('.dropdown-toggle', this).trigger('click').blur();
				});
			} else {
				$('.navbar .dropdown').off('mouseover').off('mouseout');
			}
		}
		toggleNavbarMethod();
		$(window).resize(toggleNavbarMethod);
		
		menudash()
		function menudash() {
			let id = 5
			$.ajax({
				url: "http://localhost:81/api/user/getrole1/" + id,
				type: "GET",
				success: function(hasil) {
					var txt = ""
					for (var i = 0; i < hasil.length; i++) {
						console.log(hasil[i])
						txt += `<div class="col-lg-4 ">`
						txt +=`<div class="service-item bg-dark rounded d-flex flex-column align-items-center justify-content-center text-center"> <div class="service-icon mb-4">"`
						txt +=`<i class="fa fa-2x fa-user-md text-white"></i>`
						txt +=`</div> <h4 class="mb-3 text-white"> ${hasil[i].name}</h4>`
						txt +=`<p class="m-0 text-white">menu test</p>`
						txt +=`<a class="btn btn-lg btn-primary rounded-pill" href="${hasil[i].url}>`
						txt +=`<i class="bi bi-arrow-right"></i>`
						txt +=`</a>`
						txt +=`</div>`
						txt +=`</div>`
					}
					$(".menuitem").append(txt)
				}
			})
		}


		$(".login").click(function() {
			let alamat = $(this).attr("title")
			$.ajax({
				url: alamat,
				type: "GET",
				datatype: "html",
				success: function(hasil) {
					$("#ModalAuth").modal('show')//modal show
					$(".isiModalAuth").html(hasil)
				}
			})
			return false
		})
	});


	// Date and time picker
	$('.date').datetimepicker({
		format: 'L'
	});
	$('.time').datetimepicker({
		format: 'LT'
	});


	// Back to top button
	$(window).scroll(function() {
		if ($(this).scrollTop() > 100) {
			$('.back-to-top').fadeIn('slow');
		} else {
			$('.back-to-top').fadeOut('slow');
		}
	});
	$('.back-to-top').click(function() {
		$('html, body').animate({ scrollTop: 0 }, 1500, 'easeInOutExpo');
		return false;
	});


	// Price carousel
	$(".price-carousel").owlCarousel({
		autoplay: true,
		smartSpeed: 1000,
		margin: 45,
		dots: false,
		loop: true,
		nav: true,
		navText: [
			'<i class="bi bi-arrow-left"></i>',
			'<i class="bi bi-arrow-right"></i>'
		],
		responsive: {
			0: {
				items: 1
			},
			992: {
				items: 2
			},
			1200: {
				items: 3
			}
		}
	});


	// Team carousel
	$(".team-carousel, .related-carousel").owlCarousel({
		autoplay: true,
		smartSpeed: 1000,
		margin: 45,
		dots: false,
		loop: true,
		nav: true,
		navText: [
			'<i class="bi bi-arrow-left"></i>',
			'<i class="bi bi-arrow-right"></i>'
		],
		responsive: {
			0: {
				items: 1
			},
			992: {
				items: 2
			}
		}
	});


	// Testimonials carousel
	$(".testimonial-carousel").owlCarousel({
		autoplay: true,
		smartSpeed: 1000,
		items: 1,
		dots: true,
		loop: true,
	});

})(jQuery);

