

hakaksesById() //untuk memanggil function

//buat function untuk get by id
function hakaksesById(){
	let id=sessionStorage.getItem("id") //kalau diclear maka session storage nya hilang
	$.ajax({
		url			: "http://localhost:81/api/menu/list/"+id,
		type		: "GET",
		success		: function(hasil){
			console.log(hasil)
			$("#iId").val(hasil.id)
			$("#iNm").val(hasil.name)
			$("#iUrl").val(hasil.url)
			$("#iParent").val(hasil.parentid)
		}
		
	})
}

$(document).ready(function(){
	function kembali(){
		$.ajax({
			url		: "http://localhost/menu",
			type	: "GET",
			datatype: "html",
			success	: function(hasil){
				$(".isiMain").html(hasil)
			}
		})
	}
	$(".btnKembali").click(function(){
		$.ajax({
			url		: "http://localhost/menu",
			type	: "GET",
			datatype: "html",
			success	: function(hasil){
				$("#ModalMenu").modal('hide')
			}
		})
		return false
	})
	
	$(".bt-update").click(function(){
		let id=sessionStorage.getItem("id") //kalau diclear maka session storage nya hilang
		var obj={}
		obj.id=id
		obj.name=$("#iNm").val()
		obj.url=$("#iUrl").val()
		obj.parent_id=$("#iParent").val()
		var myJson = JSON.stringify(obj)
		$.ajax({
			url  		: "http://localhost:81/api/menu/update",
			type        : "PUT",
			contentType : "application/json",
			data 		: myJson,
			success		: function(hasil){
				$("#ModalMenu").modal('hide')
				swal({
				  title: "Success!",
				  text: "Update Success!",
				  icon: "success",
				  buttons: true,
				});
				kembali()
				 //window.open("/product")
				
				}
				
		})
	})
})