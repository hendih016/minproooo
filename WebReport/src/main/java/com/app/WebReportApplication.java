package com.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebReportApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebReportApplication.class, args);
	}

}
