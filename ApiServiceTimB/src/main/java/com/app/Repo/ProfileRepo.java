package com.app.Repo;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.Model.BiodataModel;

public interface ProfileRepo extends JpaRepository<BiodataModel, Long> {
	@Query(value = "select mb.id, mb.fullname, mb.mobile_phone as mphone,mb.created_on as createdon, mu.email, mu.password, to_char(mc.dob,'yyyy-mm-dd') as dob "
			+ " from m_biodata mb join m_user mu "
			+ " on mb.id = mu.biodata_id"
			+ " join m_customer mc "
			+ " on mb.id = mc.biodata_id where mb.id= :id", nativeQuery = true)
	List<Map<String, Object>> listprofile(Long id);
	
	@Query(value = "select mb.id, mb.fullname, mb.mobile_phone as mphone, mb.modified_on as modifiedon, to_char(mc.dob,'yyyy-mm-dd') as dob "
			+ " from m_biodata mb join m_user mu "
			+ " on mb.id = mu.biodata_id"
			+ " join m_customer mc "
			+ " on mb.id = mc.biodata_id set mb.fullname= :fname,mphone= :mphone,dob= :dob", nativeQuery = true)
	List<Map<String, Object>> updatedatapribadi(String fname,String mphone,String dob);
	
	@Query(value = "select * from m_biodata where id = :id limit 1",nativeQuery = true)
	BiodataModel listprofiletahun (long id);
	
	@Query(value = "select * from m_biodata order by id", nativeQuery = true)
	List<BiodataModel> listprofile2();
}
