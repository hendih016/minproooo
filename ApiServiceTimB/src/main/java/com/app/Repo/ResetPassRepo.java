package com.app.Repo;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.Model.ResetPassModel;

@Transactional
public interface ResetPassRepo extends JpaRepository<ResetPassModel, Long> {
	
	@Query(value="select id, old_password,new_password,"
			+ "reset_for from t_reset_password where is_delete=false",nativeQuery = true)
	List<Map<String, Object>>listReset();

}
