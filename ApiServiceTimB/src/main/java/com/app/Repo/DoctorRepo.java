package com.app.Repo;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.Model.DoctorModel;

@Transactional
public interface DoctorRepo extends JpaRepository<DoctorModel, Long> {

	@Query(value = "select l.id, concat(ll.name,' ',l.name) namalokasi from m_location l\r\n"
			+ "join m_location_level ll ON ll.id = l.location_level_id\r\n", nativeQuery = true)
	List<Map<Long, Object>> popLocs();

	@Query(value = "select b.id, b.fullname from m_doctor d \r\n"
			+ "join m_biodata b ON b.id = d.biodata_id", nativeQuery = true)
	List<Map<Long, Object>> popName();

	@Query(value = "select distinct(s.name), s.id specialisasi from m_doctor d\r\n"
			+ "join t_current_doctor_specialization cds on cds.doctor_id = d.id\r\n"
			+ "join m_specialization s on s.id = cds.specialization_id\r\n", nativeQuery = true)
	List<Map<Long, Object>> popSpec();

	@Query(value = "SELECT distinct(dt.name), d.id from m_doctor d\r\n"
			+ "join t_doctor_treatment dt on d.id = dt.doctor_id", nativeQuery = true)
	List<Map<Long, Object>> popMedt();

	@Query(value = "SELECT distinct(dt.name), cds.specialization_id from m_doctor d\r\n"
			+ "	join t_doctor_treatment dt on d.id = dt.doctor_id \r\n"
			+ "	join t_current_doctor_specialization cds on cds.doctor_id = d.id\r\n"
			+ "	WHERE cds.specialization_id = :spec\r\n", nativeQuery = true)
	List<Map<Long, Object>> popMedtBySpec(Long spec);

//	@Query(value = "SELECT d.id as id, b.fullname, s.name as specname, mf.name as officename, (\r\n"
//			+ "Select EXTRACT(YEAR FROM now())-TO_NUMBER(end_year,'9999') as tahun from m_doctor_education WHERE id=D.ID AND is_last_education = true\r\n"
//			+ ") FROM m_doctor d\r\n" + "JOIN m_biodata b ON d.biodata_id = b.id\r\n"
//			+ "JOIN m_doctor_education de ON d.id = de.doctor_id\r\n"
//			+ "JOIN t_current_doctor_specialization cds ON d.id = cds.doctor_id\r\n"
//			+ "JOIN m_specialization s ON cds.specialization_id = s.id\r\n"
//			+ "JOIN t_doctor_office dof ON d.id = dof.doctor_id\r\n"
//			+ "JOIN m_medical_facility mf ON dof.medical_facility_id = mf.id", nativeQuery = true)
//	List<Map<Long, Object>> listDoctor();

	@Query(value = "SELECT DISTINCT(d.id) as id FROM m_doctor d\r\n"
			+ "JOIN m_biodata b ON d.biodata_id = b.id\r\n"
			+ "JOIN m_doctor_education de ON d.id = de.doctor_id\r\n"
			+ "JOIN t_current_doctor_specialization cds ON d.id = cds.doctor_id\r\n"
			+ "JOIN m_specialization s ON cds.specialization_id = s.id\r\n"
			+ "JOIN t_doctor_office dof ON d.id = dof.doctor_id\r\n"
			+ "JOIN t_doctor_office_treatment dot ON dot.doctor_office_id = dof.id\r\n"
			+ "JOIN t_doctor_treatment dt ON dt.id = dot.doctor_treatment_id\r\n"
			+ "JOIN m_medical_facility mf ON mf.id = dof.medical_facility_id\r\n"
			+ "JOIN m_location l ON l.id = mf.location_id\r\n"
			+ "JOIN m_location_level ll ON ll.id = l.location_level_id\r\n"
			+ "WHERE d.is_delete = false AND b.is_delete = false AND de.is_delete = false AND cds.is_delete = false AND s.is_delete = false AND dof.is_delete = false AND dot.is_delete = false AND dt.is_delete = false AND mf.is_delete = false AND l.is_delete = false\r\n"
			+ "AND s.id = :specId\r\n"
			+ "AND LOWER(b.fullname) LIKE LOWER(CONCAT('%',:name,'%')) \r\n"
			+ "AND CASE WHEN :medtId = 0 THEN dt.id = ANY(SELECT dt1.id from t_doctor_treatment dt1) ELSE dt.id = :medtId END\r\n"
			+ "AND CASE WHEN :locId = 0 THEN l.id = ANY(SELECT l1.id from m_location l1) ELSE l.id = :locId END", nativeQuery = true)
	List<Map<Long, Object>> cariDoctor(Long specId, String name, Long medtId, Long locId);

	@Query(value = "SELECT distinct(d.id) as doctorid, l.id locId, CONCAT(mf.name,' (',ll.abbreviation,' ',l.name,', ',(SELECT CONCAT(ll1.abbreviation,' ',l1.name)\r\n"
			+ "																						  FROM m_location l1 \r\n"
			+ "																						  JOIN m_location_level ll1 ON l1.location_level_id = ll1.id \r\n"
			+ "																						  WHERE l1.id = l.parent_id),')') LOCATION FROM m_doctor d\r\n"
			+ "JOIN m_biodata b ON d.biodata_id = b.id\r\n"
			+ "JOIN m_doctor_education de ON d.id = de.doctor_id\r\n"
			+ "JOIN t_current_doctor_specialization cds ON d.id = cds.doctor_id\r\n"
			+ "JOIN m_specialization s ON cds.specialization_id = s.id\r\n"
			+ "JOIN t_doctor_office dof ON d.id = dof.doctor_id\r\n"
			+ "JOIN t_doctor_office_treatment dot ON dot.doctor_office_id = dof.id\r\n"
			+ "JOIN t_doctor_treatment dt ON dt.id = dot.doctor_treatment_id\r\n"
			+ "JOIN m_medical_facility mf ON mf.id = dof.medical_facility_id\r\n"
			+ "JOIN m_location l ON l.id = mf.location_id\r\n"
			+ "JOIN m_location_level ll ON ll.id = l.location_level_id\r\n"
			+ "WHERE d.is_delete = false AND b.is_delete = false AND de.is_delete = false AND cds.is_delete = false AND s.is_delete = false AND dof.is_delete = false AND dot.is_delete = false AND dt.is_delete = false AND mf.is_delete = false AND l.is_delete = false\r\n"
			+ "AND d.id = :idDoctor", nativeQuery = true)
	List<Map<Long, Object>> ListLocation(Long idDoctor);
	
	@Query(value = "SELECT DISTINCT(D.ID), b.fullname, s.name spesialisasi, (SELECT DATE_PART('YEAR', AGE(CASE WHEN(SELECT count(id) FROM t_doctor_office dof1 WHERE dof1.deleted_on IS NULL) > 0 THEN NOW()\r\n"
			+ "	 ELSE max(dof1.deleted_on)\r\n"
			+ "END,min(dof1.created_on))) from t_doctor_office dof1 join m_doctor d1 on dof1.doctor_id=d1.id where d1.id=:idDoctor) pengalaman FROM m_doctor d\r\n"
			+ "JOIN m_biodata b ON d.biodata_id = b.id\r\n"
			+ "JOIN m_doctor_education de ON d.id = de.doctor_id\r\n"
			+ "JOIN t_current_doctor_specialization cds ON d.id = cds.doctor_id\r\n"
			+ "JOIN m_specialization s ON cds.specialization_id = s.id\r\n"
			+ "JOIN t_doctor_office dof ON d.id = dof.doctor_id\r\n"
			+ "JOIN m_medical_facility mf ON mf.id = dof.medical_facility_id\r\n"
			+ "JOIN m_location l ON l.id = mf.location_id\r\n"
			+ "JOIN m_location_level ll ON ll.id = l.location_level_id\r\n"
			+ "WHERE d.is_delete = false AND b.is_delete = false AND de.is_delete = false AND cds.is_delete = false AND s.is_delete = false AND dof.is_delete = false AND mf.is_delete = false AND l.is_delete = false\r\n"
			+ "AND d.id = :idDoctor", nativeQuery = true)
	List<Map<Long, Object>> ListDataDoctor(Long idDoctor);
}
