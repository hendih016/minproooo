package com.app.Repo;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.Model.MedicalFacilityModel;

@Transactional
public interface MedicalFacilityRepo extends JpaRepository<MedicalFacilityModel, Long>{

}
