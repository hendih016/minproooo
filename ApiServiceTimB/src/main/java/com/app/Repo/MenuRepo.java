package com.app.Repo;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.app.Model.MenuModel;


@Transactional
public interface MenuRepo extends JpaRepository<MenuModel, Long>{
	@Query(value = "select * from m_menu where is_delete=false order by id", nativeQuery = true)
	List<MenuModel> listmenu();
	
	@Query(value = "select mmr.id, mm.name from m_menu mm join m_menu_role mmr "
			+ "on mmr.menu_id=mm.id "
			+ "join m_role mr on mmr.role_id=mr.id ", nativeQuery = true)
	List<MenuModel> listmenu1();
	
	@Query(value = "select * from m_menu where id= :id limit 1", nativeQuery = true)
	MenuModel menubyid(long id);
	
	@Modifying
	@Query(value = "update m_menu set is_delete=true where id= :id ", nativeQuery = true)
	int deletemenu(long id);

}
