package com.app.Repo;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.app.Model.MenuModel;
import com.app.Model.MenuRoleModel;

@Transactional
public interface MenuRoleRepo extends JpaRepository<MenuRoleModel, Long>{
	@Query(value = "select * from m_menu_role where is_delete=false order by id", nativeQuery = true)
	List<MenuRoleModel> listmenurole();
	
	@Query(value = "select DISTINCT mm.id, mm.name,mm.parent_id from m_menu_role mmr right join m_menu mm "
			+ "on mmr.menu_id=mm.id "
			+ "left join m_role mr on mmr.role_id=mr.id order by mm.parent_id, mm.name", nativeQuery = true)
	List<Map<String, Object>> listmenu1();
	
	@Query(value = "select DISTINCT mm.id, mm.name,mm.parent_id from m_menu_role mmr join m_menu mm "
			+ "on mmr.menu_id=mm.id "
			+ "join m_role mr on mmr.role_id=mr.id where mm.parent_id= :pid", nativeQuery = true)
	List<Map<String, Object>> listmenuparent(long pid);
	
	@Query(value = "select mm.name,mm.id from m_menu_role mmr join m_menu mm "
			+ "on mmr.menu_id=mm.id "
			+ "join m_role mr on mmr.role_id=mr.id "
			+ "where mr.id= :id and mmr.is_delete=false ", nativeQuery = true)
	List<Map<String, Object>> listmenuchecked(long id);
	

	@Query(value = "select mm.name,mm.id from m_menu_role mmr join m_menu mm "
			+ "on mmr.menu_id=mm.id " 
			+ "join m_role mr on mmr.role_id=mr.id "
			+ "where mr.id= :id and mm.id= :idmenu ", nativeQuery = true)
	List<Map<String, Object>> listmenucheckedbyadmin(long id, long idmenu);
	
	@Modifying
	@Query(value = "update m_menu_role set is_delete=true where role_id= :id", nativeQuery = true)
	int updatemenu(long id);
	
	@Query(value = "select * from m_menu_role where id= :id limit 1", nativeQuery = true)
	MenuRoleModel menurolebyid(long id);
	
	@Modifying
	@Query(value = "update m_menu_role set is_delete=true where id= :id ", nativeQuery = true)
	int deletemenurole(long id);

}
