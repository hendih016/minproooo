package com.app.Repo;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.Model.SpecializationModel;

@Transactional
public interface SpecializationRepo extends JpaRepository<SpecializationModel, Long>{
	@Query(value = "select id,name from m_specialization where is_delete=false",nativeQuery = true)
	List<Map<String, Object>> list();
}
