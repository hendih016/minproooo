package com.app.Repo;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.app.Model.userModel;


@Transactional
public interface userRepo extends JpaRepository<userModel, Long> {

	@Query(value="select * from m_user",nativeQuery = true)
	List<userModel>userorder();
	
	@Query(value = "select id, biodata_id, role_id, email, password,"
			+ " login_attempt, is_locked, last_login from m_user where is_delete=false", nativeQuery = true)
	List<Map<String, Object>>list_User();
	
	@Query(value = "select id from m_user order by id desc limit 1", nativeQuery = true)
	int selectById();
	
	@Query(value = "select mu.id,mu.email,mu.biodata_id,mb.fullname,mu.password from m_user mu join m_biodata mb on mb.id=mu.biodata_id where mu.biodata_id= :id limit 1", nativeQuery = true)
	Map<String, Object> selectbybiodataid(long id);
	
	@Query(value = "update m_user set email= :em where biodata_id= :id limit 1", nativeQuery = true)
	int updateemail(String em,long id);
	
	
	@Modifying
	@Query(value = "update m_user set created_by= :id where id= :id", nativeQuery = true)
	int setDefaultCreatedBy(long id);
	
	//mengecek email sudah ada atau blm
	@Query(value = "select coalesce(sum(id),0) from m_user where email = :email and is_delete=false ", nativeQuery = true)
	int cek_Email(String email);
	
	//membuat password baru
	@Modifying
	@Query(value="update m_user set password = :ps ,modified_by=1, modified_on = now()  where email = :em ",nativeQuery = true)
	void setPasswordbaru(String ps, String em);
	
	
	
	//mengecek password sama apa belum
	@Query(value = "select coalesce(sum(id),0) from m_user where password = :ps and email= :em  and is_delete=false ", nativeQuery = true)
	int cek_pass(String ps, String em);
	

	/*
	 * //mengecek password sama apa belum
	 * 
	 * @Query(value =
	 * "select password from m_user where password = :ps and is_delete=false and id= :id "
	 * , nativeQuery = true) int cek_currentpass(String ps, long id);
	 */
	
	@Query(value=" select password from m_user where email= :em and is_delete=false limit 1 ", nativeQuery = true)
	String oldpass(String em);
	
	@Query(value="select email from m_user where id= :id AND is_delete=false limit 1",nativeQuery = true)
	String getemail(long id);
	
	@Query(value="select login_attempt from m_user where  email= :em",nativeQuery = true)
	Map<String, Object> cekLoginAttempt(String em);
	
	
	//ceklogin
	@Query(value=" select u.id,u.email,b.Fullname as Fullname, u.role_id as roleid, u.biodata_id as biodataid from m_user u join m_biodata b on u.biodata_id=b.id join m_role r on r.id=u.role_id where email= :em and password = :ps and u.is_delete=false and u.login_attempt!=3 and u.is_locked = false limit 1 ", nativeQuery = true)
	Map<String, Object> ceklogin(String em, String ps);
	
	@Modifying
	@Query(value = "update m_user set is_locked=true where login_attempt=3 ",nativeQuery = true)
	void updateAttempt();
	
	@Modifying
	@Query(value = "update m_user set login_attempt = login_attempt+1 where email= :em",nativeQuery = true)
	void LoginAttempt(String em);
	
	@Modifying
	@Query(value = "update m_user set login_attempt = 0 where email= :em",nativeQuery = true)
	void updateAttemptreset(String em);
	
	
	
	
	//cekrole
	@Query(value = "select u.id as userid,m.url,m.name,u.role_id,m.small_icon from m_user u join m_biodata b on b.id=u.biodata_id "
			+ "join m_role r on r.id=u.role_id join m_menu_role mr on mr.role_id=r.id join m_menu m on m.id=mr.menu_id where u.biodata_id= :id and mr.is_delete=false"
			, nativeQuery = true)
	List<Map<String, Object>> getrole(long id);
	
	//cekrole
		@Query(value = "select m.url,m.name from "
				+ " m_role r join m_menu_role mr on mr.role_id=r.id join m_menu m on m.id=mr.menu_id where r.id= :id"
				, nativeQuery = true)
		List<Map<String, Object>> getrole1(long id);
}
