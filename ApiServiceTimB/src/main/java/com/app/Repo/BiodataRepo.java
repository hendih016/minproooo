package com.app.Repo;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.Model.BiodataModel;

@Transactional
public interface BiodataRepo extends JpaRepository<BiodataModel, Long>{

	//memanggil database Biodata
	@Query(value = "select id, fullname, mobile_phone, image_path"
			+ " from m_biodata where is_delete=false", nativeQuery = true)
	List<Map<String, Object>>listBiodata();
	
	//memanggi id biodata yang terbaru dibuat
	@Query(value = "select Max(id) from m_biodata limit 1", nativeQuery = true)
	int selectById();
}
