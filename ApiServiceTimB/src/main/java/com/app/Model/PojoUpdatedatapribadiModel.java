package com.app.Model;

import java.sql.Date;

public class PojoUpdatedatapribadiModel {
	private long biodata_id;
	private String fullname;
	private String mobile_phone;
	private long blood_group_id;
	private long customer_id;
	private Date dob;
	
	public PojoUpdatedatapribadiModel(long biodata_id, String fullname, String mobile_phone, long blood_group_id,
			long customer_id, Date dob) {
		super();
		this.biodata_id = biodata_id;
		this.fullname = fullname;
		this.mobile_phone = mobile_phone;
		this.blood_group_id = blood_group_id;
		this.customer_id = customer_id;
		this.dob = dob;
	}

	public long getBiodata_id() {
		return biodata_id;
	}

	public void setBiodata_id(long biodata_id) {
		this.biodata_id = biodata_id;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getMobile_phone() {
		return mobile_phone;
	}

	public void setMobile_phone(String mobile_phone) {
		this.mobile_phone = mobile_phone;
	}

	public long getBlood_group_id() {
		return blood_group_id;
	}

	public void setBlood_group_id(long blood_group_id) {
		this.blood_group_id = blood_group_id;
	}

	public long getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(long customer_id) {
		this.customer_id = customer_id;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}
	
	

}
