package com.app.Model;

import java.util.Date;

public class PojoTambahPasienModel {
	
	private String fullname;
	private Date dob;
	private String gender;
	private long blood_group_id;
	private  String rhesus_type;
	private double height;
	private double weight;
	public long customer_relation_id;
	
	


	public String getGender() {
		return gender;
	}


	public Date getDob() {
		return dob;
	}


	public void setDob(Date dob) {
		this.dob = dob;
	}


	public void setGender(String gender) {
		this.gender = gender;
	}


	public long getBlood_group_id() {
		return blood_group_id;
	}


	public void setBlood_group_id(long blood_group_id) {
		this.blood_group_id = blood_group_id;
	}


	public String getRhesus_type() {
		return rhesus_type;
	}


	public void setRhesus_type(String rhesus_type) {
		this.rhesus_type = rhesus_type;
	}


	public double getHeight() {
		return height;
	}


	public void setHeight(double height) {
		this.height = height;
	}


	public double getWeight() {
		return weight;
	}


	public void setWeight(double weight) {
		this.weight = weight;
	}


	public long getCustomer_relation_id() {
		return customer_relation_id;
	}


	public void setCustomer_relation_id(long customer_relation_id) {
		this.customer_relation_id = customer_relation_id;
	}


	public String getFullname() {
		return fullname;
	}
	
	
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
}
