package com.app.Controller;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.Model.bankModel;
import com.app.Repo.bankRepo;


@RestController
@RequestMapping("api/bank")
@CrossOrigin(origins="*")
public class bankControl {

	@Autowired
	private bankRepo br;
	
	public int deleteValid = 0;

	@GetMapping("list")
	public List<bankModel>bankorder(){
		return br.bankorder();
	}
	
	@GetMapping("listbyName/{nm}")
	public List<bankModel> bankByNama(@PathVariable String nm){
		return br.bankByNama(nm);
	}
	
	@GetMapping("listbyKodeName/{nm}/{va}")
	public List<bankModel> bankByNamaKode(@PathVariable String nm,
											@PathVariable String va)
	{
		return br.bankByNamaKode(nm, va);
	}
	
	
	@GetMapping("listbyKode/{va}")
	public List<bankModel> bankByKode(@PathVariable String va){
		return br.bankByKode(va);
	}
	
	
	@GetMapping("list/{id}")
	public bankModel bankById(@PathVariable long id) {
		return br.bankById(id);
	}
	
	@PostMapping("add")
	public void addBank(@RequestBody bankModel bm) {
		LocalDateTime today =  LocalDateTime.now();
		bm.setId(br.getlastid()+1);
		bm.setCreated_on(today);
		bm.setCreated_by((long) 1);;
		bm.setModified_by(0);
		bm.setModified_on(today);
		bm.setDeleted_on(today);
		bm.setDeleteded_by(0);
		bm.setIs_delete(false);
		br.save(bm);
	}
	
	@GetMapping("count/{nm}")
	public Long getCountid(@PathVariable String nm) {
		
		return br.getCountid(nm);
	}
	

	@PutMapping("edit")
	public void editBank(@RequestBody bankModel bm) {
		/*
		 * LocalDateTime today = LocalDateTime.now(); 
		 * bm.setModified_on(today);
		 * bm.setModified_by(1);
		 */
		br.save(bm);
	}
	
	@DeleteMapping("delete")
	public void deleteBank(@RequestBody bankModel bm) {
		LocalDateTime today =  LocalDateTime.now();
		
		if(deleteValid == 0) {
		bm.setDeleted_on(today);
		bm.setDeleteded_by(1);
		
		br.deleteBank(bm.getId());
		}else {
			System.out.println("data masih dipakai");
		}
	}
	
	
	

}
