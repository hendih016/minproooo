package com.app.Controller;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.Model.ResetPassModel;
import com.app.Repo.ResetPassRepo;
import com.app.Repo.userRepo;
import com.app.com.MailConfig;

@RestController
@RequestMapping("api/resetpassword")
@CrossOrigin(origins = "*")
public class ResetPassControl {
	@Autowired
	private ResetPassRepo rpr;

	@Autowired
	private userRepo ur;
	
	@GetMapping("list")
	public List<Map<String, Object>>listReset(){
		return rpr.listReset();
	}
	
	@PostMapping("post")
	public void post(@RequestBody ResetPassModel rpm) {
		rpm.setResetfor("ubah password");
		rpm.setCreated_on(LocalDateTime.now());
		rpm.setCreated_by(0);
		rpm.setIs_delete(false);
		rpr.save(rpm);
	}

}
