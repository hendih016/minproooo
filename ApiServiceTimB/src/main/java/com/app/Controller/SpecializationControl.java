package com.app.Controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.Repo.SpecializationRepo;
@RestController
@RequestMapping("api/specialization")
@CrossOrigin(origins = "*")
public class SpecializationControl {
	@Autowired
	private SpecializationRepo sr;
	
	@GetMapping("list")
	public List<Map<String, Object>> list(){
		return sr.list();
	}
}
