package com.app.Controller;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.app.Model.LevelLokasiModel;
import com.app.Model.LokasiModel;
import com.app.Repo.LevelLokasiRepo;
import com.app.Repo.LokasiRepo;

@RestController
@RequestMapping("api/lokasi")
@CrossOrigin(origins = "*")
public class LokasiControl {

	@Autowired
	private LokasiRepo lr;

	public int deleteValid = 0;

	@GetMapping("list")
	public List<Map<Long, Object>> listLevelLokasi() {
		return lr.listLokasi();
	}
	
	@GetMapping("list/{name}")
	public List<Map<Long, Object>> listLevelLokasi(@PathVariable String name) {
		return lr.listLokByName(name);
	}

	@GetMapping("listbynamelev")
	public List<Map<Long, Object>> carilokasilevel(@RequestParam String name, @RequestParam Long idlev) {
		return lr.cariLokByNameLev(name, idlev);
	}
	
	@GetMapping("listByID/{id}")
	public List<Map<Long, Object>> listLevelLokasiById(@PathVariable Long id) {
		return lr.listLokasiById(id);
	}

	@PostMapping("add")
	public void addProduct(@RequestBody LokasiModel lm) {
		lr.addLevLok(lm.getId(), lm.getName(), lm.getLocationLevelId(), lm.getParentId(), lm.getC_by());
	}
	
	@DeleteMapping("delete")
	public void deleteLevLok(@RequestBody LokasiModel lm) {
		LocalDateTime today = LocalDateTime.now();
		lm.setD_on(today);
		lm.setD_by((long) 1);

		lr.deleteLok(lm.getId());
	}

	@PutMapping("update")
	public void editLevLok(@RequestBody LokasiModel lm) {
		LocalDateTime today = LocalDateTime.now();
		lm.setM_on(today);
		lm.setM_by((long) 1);

		lr.save(lm);
	}

}
