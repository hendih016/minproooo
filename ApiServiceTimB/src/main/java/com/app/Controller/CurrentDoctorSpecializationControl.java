package com.app.Controller;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.Model.BloodGroupModel;
import com.app.Model.CurrentDoctorSpecializationModel;
import com.app.Repo.BloodGroupRepositorys;
import com.app.Repo.CurrentDoctorSpecializationRepo;

@RestController
@RequestMapping("api/currentdoctor")
@CrossOrigin(origins = "*")
public class CurrentDoctorSpecializationControl {
	@Autowired
	private CurrentDoctorSpecializationRepo cdsr;
	
	@GetMapping("list")
	public List<Map<String, Object>> listCurrentSpecializationActive() {
		return cdsr.listCurrentSpecializationActive();
	}
	
	@GetMapping("list/{id}")
	public Map<String, Object> currentSpecializationById(@PathVariable long id) {
		return cdsr.currentSpecializationById(id);
	}
	@GetMapping("listbybiodata/{id}")
	public Map<String, Object> currentSpecializationBybiodataId(@PathVariable long id) {
		return cdsr.currentSpecializationBybiodataId(id);
	}
	@PostMapping("add")
	public void addcurrentSpesialis(@RequestBody CurrentDoctorSpecializationModel cdsm) {
		LocalDateTime today = LocalDateTime.now();
		cdsm.setD_by(0l);
		cdsm.setD_on(today);
		cdsm.setIs_del(false);
		cdsm.setM_by(0l);
		cdsm.setM_on(today);
		cdsm.setC_on(today);
		cdsm.setC_by((long)1);
		cdsr.save(cdsm);
		
	}

	@PutMapping("edit")
	public void editSpesialis( @RequestBody CurrentDoctorSpecializationModel cdsm) {
		CurrentDoctorSpecializationModel data = cdsr.findById(cdsm.getId()).get();
		LocalDateTime today = LocalDateTime.now();
		data.setM_on(today);
		data.setM_by((long)1);
		data.setDoctorId(cdsm.getDoctorId());
		data.setSpecializationId(cdsm.getSpecializationId());
		data.setId(cdsm.getId());
		cdsr.save(data);
	}

}
