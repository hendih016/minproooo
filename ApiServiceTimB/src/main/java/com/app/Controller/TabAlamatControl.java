package com.app.Controller;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.Model.TabAlamatModel;
import com.app.Repo.TabAlamatRepo;


@RestController
@RequestMapping("api/tabalamat")
@CrossOrigin(origins = "*")
public class TabAlamatControl {

	@Autowired
	private TabAlamatRepo tar;
	
	@GetMapping("listalamat/{biodataid}")
	public List<TabAlamatModel> listTabAlamatActive(@PathVariable long biodataid) {
		return tar.listTabAlamatActive(biodataid);
	}
	
	@GetMapping("list/{id}")
	public TabAlamatModel tabalamatbyid(@PathVariable long id) {
		return tar.tabalamatbyid(id);
	}
	
	@PostMapping("add")
	public void addTabAlamat(@RequestBody TabAlamatModel tam) {
		LocalDateTime today = LocalDateTime.now();
		tam.setCreatedby(tam.getId());
		tam.setCreatedon(today);
		tar.save(tam);
	}
	
	@PutMapping("update")
	public void updateTabAlamat(@RequestBody TabAlamatModel tam) {
		LocalDateTime today = LocalDateTime.now();
		tam.setModifiedby(tam.getId());
		tam.setModifiedon(today);
		tar.save(tam);
	}
	
	@DeleteMapping("delete")
	public void deletevariants(@RequestBody TabAlamatModel tam) {
		LocalDateTime today = LocalDateTime.now();
		tam.setDeletedby(tam.getId());
		tam.setDeletedon(today);
		tar.deleteTabAlamat(tam.getId());
	}
	
	@GetMapping("search/{biodataid}/{alamat}")
	public List<TabAlamatModel> searchTabAlamat(@PathVariable String alamat, @PathVariable long biodataid) {
		return tar.searchTabAlamat(alamat, biodataid);
	}
	
	@GetMapping("listkotakec")
	public List<Map<String, Object>> lokasilist() {
		return tar.listlokasi();
	}
}
