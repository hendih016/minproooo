package com.app.Controller;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.Model.MenuRoleModel;
import com.app.Model.userModel;
import com.app.Repo.MenuRoleRepo;


@RestController
@RequestMapping("api/menurole")
@CrossOrigin(origins = "*")
public class MenuRoleRestControl {
	@Autowired
	private MenuRoleRepo mrr;
	
	@GetMapping("list")
	public List<MenuRoleModel> listmenurole(){
		return mrr.listmenurole();
	}
	
	@GetMapping("listmenu1")
	public List<Map<String, Object>> listmenurole1(){
		return mrr.listmenu1();
	}
	@GetMapping("listmenuparent/{pid}")
	public List<Map<String, Object>> listmenuparent(@PathVariable long pid){
		return mrr.listmenuparent(pid);
	}
	
	@GetMapping("listmenu1/{id}")
	public List<Map<String, Object>> listmenurolechecked(@PathVariable long id){
		return mrr.listmenuchecked(id);
	}
	@PostMapping("postmenu")
	public void menurole(@RequestBody MenuRoleModel mrm){
		mrr.save(mrm);
	}
	@PutMapping("updatemenu")
	public void updateemail(@RequestBody MenuRoleModel mrm) {
		mrr.save(mrm);
	}
	@GetMapping("cekmenu/{id}/{idmenu}")
	public List<Map<String, Object>> listmenuchek(@PathVariable long id,@PathVariable long idmenu) {
		return mrr.listmenucheckedbyadmin(id, idmenu);
	}
	@PostMapping("add")
	public void addmenurole( @RequestBody MenuRoleModel mrm) {
		LocalDateTime today = LocalDateTime.now();
		mrm.setCreateon(today);
		mrm.setCreateby(1);
		mrr.save(mrm);
	}
	
	@GetMapping("list/{id}")
	public MenuRoleModel menuroleById(@PathVariable long id){
		return mrr.menurolebyid(id);
	}
	@PostMapping("addmenu")
    public void addAlamat(@RequestBody MenuRoleModel mrm) {
        LocalDateTime today = LocalDateTime.now();
        mrm.setCreateby(mrm.getId());
        mrm.setCreateon(today);
        mrr.save(mrm);
    }

    @PutMapping("updatemenurole")
    public void update(@RequestBody MenuRoleModel mrm){
        LocalDateTime today = LocalDateTime.now();
        mrm.setCreateon(mrm.getCreateon());
        mrm.setCreateby(mrm.getId());
        mrm.setModifiedby(mrm.getId());
        mrm.setModifiedon(today);
        mrm.setDeleteby(mrm.getId());
        mrm.setDeleteon(today);
        mrr.updatemenu(mrm.getRoleid());
    }
	@PutMapping("update")
	public void updatemenurole(@RequestBody MenuRoleModel mrm) {
		LocalDateTime today = LocalDateTime.now();
		mrm.setModifiedon(today);
		mrm.setModifiedby(1);
		mrr.save(mrm);
	}
	
	@PostMapping("updatechecked")
	public void updatechecked (@RequestBody List<Map<String, String>> dt) {
		System.out.println(dt);
	}
	
	@DeleteMapping("delete")
	public void deletemenurole(@RequestBody MenuRoleModel mrm){
		LocalDateTime today = LocalDateTime.now();
		mrm.setDeleteon(today);
		mrm.setModifiedby(1);
		mrr.deletemenurole(mrm.getId());
	}
}
