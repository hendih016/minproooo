package com.app.Controller;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.Model.BiodataModel;
import com.app.Model.CustomerModel;
import com.app.Model.PojoUpdatedatapribadiModel;
import com.app.Repo.BiodataRepo;
import com.app.Repo.CustomerRepo;
//import com.app.Repo.CustomerRepo;
import com.app.Repo.ProfileRepo;

@RestController
@RequestMapping("api/tabprofile")
@CrossOrigin(origins = "*")
public class TabProfileControl {

	@Autowired
	private ProfileRepo pr;
	
	@Autowired
	private BiodataRepo br;
	
	@Autowired
    private CustomerRepo cr;
	
	@GetMapping("listprofile/{id}")
	public List<Map<String, Object>> listprofile(@PathVariable long id){
		return pr.listprofile(id);
	}
	
	@GetMapping("listprofiletahun/{id}")
	public BiodataModel listprofilethn(@PathVariable long id){
		return pr.listprofiletahun(id);
	}
	
	@GetMapping("list2")
	public List<BiodataModel> listprofil2(){
		return pr.listprofile2();
	}

	@PutMapping("editdatapribadi")
	public void editcs(@RequestBody PojoUpdatedatapribadiModel pupm) {
			// update biodata
			BiodataModel bm = br.findById(pupm.getBiodata_id()).get();
			bm.setFullname(pupm.getFullname());
			LocalDateTime today = LocalDateTime.now();
			bm.setM_on(today);
			bm.setM_by((long) 1);
			bm.setMobile_phone(pupm.getMobile_phone());
			br.save(bm);
			// save customer
			CustomerModel cm = cr.findById(pupm.getBiodata_id()).get();
			cm.setModifedOn(today);
			cm.setModifedBy((long) 1);
			cm.setDob(pupm.getDob());
			cr.save(cm);
	
	}

}
